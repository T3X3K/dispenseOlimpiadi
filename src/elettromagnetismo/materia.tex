\subsection{I campi nella materia}
\subsubsection{Elettrostatica dei dielettrici lineari}
Spesso questo argomento viene trattato in modo approssimativo in quanto è estremamente complicato anche per i modelli più semplici che si possono fare per la risposta di un materiale all'applicazione di un campo elettrico esterno.

I modelli che si fanno sono \textbf{tutti} approssimati. Noi vedremo un solo tipo di approssimazione, che è il più comune ed è l'unico che può capitarvi in un problema olimpico in quanto le cose si fanno rapidamente molto difficili. 

In qualche modo bisogna schematizzare la reazione di un materiale sotto l'effetto di un campo esterno. Per fare un modello è necessario andare a vedere la cella fondamentale della materia, ovvero l'atomo. Tutti i modelli che faremo saranno riferiti a campi \textit{deboli}, ovvero campi non abbastanza intensi per esempio da ionizzare gli atomi.

Il buonsenso ci dice che sotto l'effetto di un campo esterno la nuvola di elettroni che ruota intorno al nucleo si sposterà  leggermente nella direzione del campo. In particolare, dato che gli elettroni hanno carica negativa, ci aspettiamo che la nuvola si sposti in verso opposto al campo. Questo sbilanciamento delle cariche in \textbf{prima approssimazione} è schematizzabile come la formazione di un dipolo elettrico di dimensioni atomiche. Il dipolo avrà la direzione del campo e stavolta anche lo stesso verso, in quanto per convenzione si prende il dipolo che va dalla carica negativa a quella positiva.

In questo modo avremo quindi che ogni atomo possiede un suo momento di dipolo
$\vec p_i$. Ovviamente a noi interesserà una trattazione macroscopica del fenomeno per cui non saremo interessati al caso locale del dipolo quanto all'effetto globale che ha sul sistema. Di conseguenza potremo definire un vettore densità di dipolo $\vec P$

\[ \vec P = \dsum_i n_i \vec p_i\]


Dove $n_i$ è la densità numerica di dipoli. In generale, dato che gli atomi e le molecole saranno diverse dentro lo stesso materiale, ci vuole una sommatoria. Questo vettore $\vec P$ ha le dimensioni di dipolo per unità di volume, proprio per la presenza di $n_i$. 

Andiamo ora a calcolare il campo elettrico, assumendo che la materia si comporti nel modo che abbiamo appena descritto. Ricordiamo che in elettrostatica vale sempre

\[ \rot \vec E =0\]

Ovvero potremo sempre scrivere $\vec E$ come gradiente del potenziale. Notare che questo vale per $\vec E$ e non per $\vec P$ o $\vec D$, il vettore che definiremo fra poco.


Supponiamo di avere quindi una certa densità di carica netta libera $\rho$ e in aggiunta una zona in cui vi è del materiale che si polarizza. Supponiamo di conoscere già il modo in cui si polarizza e quindi di conoscere il vettore $\vec P$ in ogni punto. Cerchiamo di calcolare il campo elettrico sfruttando il principio di sovrapposizione, ovvero dicendo che il campo elettrico totale sarà la somma del termine noto dovuto alle cariche libere e del campo generato dai vari dipoli.


Innanzitutto, sappiamo che

\[\vec E = -\grad \phi \]

Proprio perchè il campo elettrostatico rimane conservativo. A questo punto, visto che calcolare il potenziale elettrico è più facile di calcolare il campo in quanto uno è un vettore e l'altro è uno scalare, calcoliamo prima il potenziale per poi arrivare al campo.

Il potenziale generato dalla carica libera sarà, secondo le formule che abbiamo già discusso a lungo, 

\[ \phi(\vec r) = \dfrac{1}{4\pi\epsilon_0}\dint_V \dfrac{\rho(\vec r')}{|\vec r - \vec r'|} \dd V'\]

Dove $V$ è il volume in cui è inclusa la carica. A questo punto calcoliamo separatamente il potenziale  generato dai dipoli. Ricordiamo che per un dipolo elettrico singolo il potenziale è 

\[\phi = \dfrac{1}{4\pi\epsilon_0}\dfrac{\vec p \cdot \b r}{r^2} = \dfrac{\vec p\cdot \vec r}{r^3}\]

Dato che $\vec P$ è una densità di dipoli, $\vec P  \dd V$ sarà a tutti gli effetti un piccolo dipolo elettrico, per cui

\[  \dd \phi = \dfrac{1}{4\pi\epsilon_0}\dfrac{\vec P \cdot(\vec r - \vec r')}{|\vec r - \vec r'|^3} \dd V'\]


Per cui il potenziale totale sarà

\[ \phi(\vec r) = \dfrac{1}{4\pi\epsilon_0}\dint_V \left(\dfrac{\rho(\vec r')}{|\vec r - \vec r'|} +\dfrac{\vec P \cdot(\vec r - \vec r')}{|\vec r - \vec r'|^3}     \right) \dd V'\]


A questo punto bisogna fare le magie per far comparire un risultato che sembri qualcosa che abbiamo già visto. Al solito tireremo fuori identità vettoriali a caso che non vi verrebbero mai in mente se non sapeste dove si vuole arrivare. Non dovete ricordarvi i passaggi, io li faccio in modo che voi possiate capire il senso delle formule finali a cui si arriva che sembrano calate dal cielo e spesso non se ne capisce il significato.


La prima cosa da notare è che

\[ \grad' \left(\dfrac{1}{|\vec r - \vec r'|}\right) = \dfrac{\vec r - \vec r'}{|\vec r - \vec r'|^3}\]

Vorrei farvi notare che a priori $\vec r = (x,y,z)$ e $\vec r' = (x',y', z')$, per cui la funzione 

\[ \dfrac{1}{|\vec r - \vec r'|}\]

È funzione di 6 variabili, $(x,y,z,x',y',z')$. Per questo è bene distinguere $\grad$ da $\grad'$. Il primo gradiente indica che si fanno le derivate rispetto alle variabili non primate trattando le altre come parametri, ovvero trattando la funzione suddetta come

\[\dfrac{1}{|\vec r - \vec r'|} = f_{\vec r'}(\vec r)\]

Ovvero una funzione di $\vec r$ dipendente da un parametro, cosa di cui voi sapete fare il gradiente. $\grad'$ sarà ovviamente il contrario. 

Continuiamo con il calcolo del potenziale


\[ \phi(\vec r) = \dfrac{1}{4\pi\epsilon_0}\dint_V \left(\dfrac{\rho(\vec r')}{|\vec r - \vec r'|} +\vec P \cdot\grad'\left(\dfrac{1}{|\vec r - \vec r'|}\right)     \right) \dd V'\]

Ora andiamo ad usare una delle identità vettoriali che usiamo di solito per far uscire una cosa utile

\[\div(f\vec G) = \grad f \cdot \vec G + f \div \vec G\]

Ovviamente sceglieremo $\vec G = \vec P $ e $f = 1/R$, per cui il potenziale diventa

\[ \phi(\vec r) = \dfrac{1}{4\pi\epsilon_0}\dint_V \left(\dfrac{\rho(\vec r')}{|\vec r - \vec r'|} + \vec \nabla ' \cdot \left(\dfrac{\vec P(\vec r')}{|\vec r - \vec r'|} \right)- \dfrac{\vec \nabla ' \cdot \vec P(\vec r')}{|\vec r - \vec r'|}\right)      \dd V'\]

A questo punto è opportuno scrivere in modo più intelligente il tutto e usare il teorema della divergenza

\begin{equation}
\phi(\vec r ) = \dfrac{1}{4\pi\epsilon_0}\dint_V \dfrac{\rho(\vec r') - \vec \nabla ' \cdot \vec P(\vec r')}{|\vec r - \vec r'|} \dd V' + \dfrac{1}{4\pi\epsilon_0}\doint_{\d V} \dfrac{\vec P(\vec r')}{|\vec r - \vec r'|}\cdot \dd\vec A' 
\label{potenziale con dipoli}
\end{equation}


Ora questo potenziale ha una forma molto più potabile perché sembra davvero qualcosa che abbiamo già visto un milione di volte. Se definiamo

\[ \rho\ped{eff} = \rho - \div \vec P\]

Allora il primo pezzo diventa

\[\dfrac{1}{4\pi\epsilon_0}\dint_V \dfrac{\rho(\vec r')\ped{eff}}{|\vec r - \vec r'|} \dd V'\]


E questa è la formula che abbiamo sempre usato per il potenziale! Il secondo pezzo diventa molto chiaro se scriviamo

\[\dd\vec A = \b n  \dd A\]

Ovvero separiamo la parte vettoriale dalla parte scalare nell'area, mettendoci un opportuno versore. In questo modo il secondo termine è 

\[\dfrac{1}{4\pi\epsilon_0}\doint_{\d V} \dfrac{\vec P \cdot \b n}{|\vec r - \vec r'|} \dd A'\]

E se diciamo che

\[\sigma = \vec P \cdot \b n\]

Allora questa è di nuovo a tutti gli effetti una densità di carica superficiale e la formula è quella che abbiamo sempre usato!

Cerchiamo di dare una breve interpretazione fisica di queste formule prima di dare un'equazione che generalizzi il teorema di Gauss quando ci troviamo in presenza di materiali.

\begin{figure}[!ht]
\centering
\includegraphics[width = 0.5 \textwidth]{immagini/dipolo_superficie.jpg}
\caption{Accumulo di carica superficiale}
\label{fig:dipolo superfici}
\end{figure}



Come vedete in figura \ref{fig:dipolo superfici} non è necessario che ci sia una carica netta nel volume affinché vi sia una certa $\sigma$ superficiale. Infatti i dipoli se si allineano creano una parte particolarmente negativa sul bordo alto e una positiva sul bordo basso. Questa è l'interpretazione del termine di integrale superficiale e $\vec P \cdot \b n$ è esattamente la quantificazione di questo termine.

Allo stesso modo, se supponiamo che i dipoli siano tutti rivolti verso un \textit{centro}, allora avremo che $\div \vec P \neq 0$ e quindi avremo una sorta di $\rho\ped{dip} $ in aggiunta alla carica libera già presente.


Riprendiamo adesso la formula \ref{potenziale con dipoli}  e cerchiamo di scriverla in modo più bello. Noi sappiamo che se il potenziale è dato dalla formula

\[\phi(\vec r) = \dfrac{1}{4\pi \epsilon_0}\dint_V \dfrac{\rho (\vec r')}{|\vec r - \vec r'| } \dd V'\]

Allora la quantità $\div \vec E$, dove $\vec E = - \grad \phi$ è esattamente

\[ \div \vec E = \dfrac{\rho}{\epsilon_0}\]


Ora per evitare confusione indichiamo $\rho$ con $\rho\ped{lib}$, in quanto quello che abbiamo chiamato $\rho$ fin'ora è esattamente la carica \textit{libera} e non la carica \textit{fasulla} simulata dall'opportuna distribuzione di dipoli. Riprendendo appunto la formula \ref{potenziale con dipoli} diventa ovvio per analogia che nel modello che abbiamo fatto la legge si riscrive

\[\div \vec E = \dfrac{\rho\ped{lib}}{\epsilon_0} - \dfrac{\div \vec P}{\epsilon_0} \]

Che possiamo semplicemente riscrivere come

\[\epsilon_0 \div \vec E + \div \vec P = \rho\ped{lib}\]


E dato che $\epsilon_0 $ è costante, si può ancora semplificare

\[\div (\epsilon_0\vec E + \vec P) = \rho\ped{lib}\]


A questo punto spesso viene definito il vettore spostamento dielettrico $\vec D$

\[\vec D = \epsilon_0 \vec E + \vec P\]

Che \textbf{ha come unico scopo quello di scrivere in forma più compatta la legge che ho appena scritto}. Il vettore $\vec D$ infatti non ha significato fisico diretto particolarmente rilevante. Il vettore $\vec E$ è quello che ci dice quanta forza ci sarà sulle cariche, il vettore spostamento dielettrico serve solo a scrivere bene le leggi che abbiamo ricavato con fatica.

A questo punto della trattazione iniziano a confondersi le cose. Quello che ho scritto fino ad adesso non è la cosa più generale possibile ma poco ci manca in quanto non ho ancora dato una relazione che leghi i vettori $\vec E$ e $\vec P$, ma ho semplicemente supposto che basti il termine di dipolo a descrivere la risposta del materiale, cosa che vale nella maggior parte dei casi. A questo punto fioccano i modelli che cercano di dare una descrizione del legame fra il campo applicato al nostro oggetto dielettrico e la sua risposta, ovvero esattamente la legge che lega $\vec E$ e $\vec P$. Il modello più semplice\footnote{Ovvero quello che capita alle Olimpiadi} è ovviamente il modello di risposta lineare, ovvero un modello che ci dice che

\begin{equation}
\vec P = \epsilon_0\chi \vec E 
\label{legame p ed E}
\end{equation}


Dove $\chi$ è sperabilmente una costante ed è uniforme, almeno in elettrostatica. A questo punto è facile anche scrivere il vettore $\vec D$

\[\vec D = (\epsilon_0 \vec E + \vec P) = \epsilon_0 (1+\chi)\vec E = \epsilon \vec E\]

Dove ovviamente $\epsilon = \epsilon_0 (1+\chi)$. Si definisce anche $\epsilon_r$, la costante dielettrica relativa tale che $\epsilon = \epsilon_0 \epsilon_r$. So di aver definito un sacco di cose con nomi simili in poche righe. Il problema è che purtroppo non esiste una convenzione universale per descrivere queste cose e ognuno ha pensato bene di farne una tutta sua, aumentando il numero di oggetti da definire. Io li riporto tutti in quanto è bene che conosciate tutti i modelli che potete incontrare, ma sono d'accordo con voi se mi dite che definire 10 oggetti simili con nomi simili e proprietà che differiscono di niente porta a confusione.


Ovviamente la relazione più generale sarà di un altro tipo, ovvero

\[D_i = \dsum_{j=1}^3 \epsilon_{ij}E_j\]

Dove la quantità $\epsilon_{ij}$ è un tensore (=matrice $3\times 3$) che permette ai vettori $\vec E$ e $\vec D$ di essere legati da una relazione più generale del parallelismo. Pensate al solito paragone del legno nodoso per capire come mai non devono necessariamente essere paralleli.

Ricordiamo che dato che

\[\div \vec D  = \rho\ped{lib}\]

Avremo che

\[ \Phi(\vec D, \d V) = Q\ped{lib}(V)\]

Ovvero il flusso del vettore spostamento dielettrico sarà uguale alla carica contenuta nella superficie.

\textbf{Attenzione}: l'equazione \ref{legame p ed E} richiede attenzione. Supponiamo di avere una sfera di dielettrico e di immergerla in un campo elettrico $\vec E\ped{ext}$ generico. Dato il campo esterno, la sfera si polarizzerà. Tuttavia, la polarizzazione della sfera andrà a modificare il campo elettrico che c'era all'inizio, per cui la polarizzazione \textbf{non} sarà $\vec P = \chi \vec E\ped{ext}$, ma sarà $\vec P = \chi \vec E_{2}$, dove $\vec E_{2} = \vec E\ped{ext} + \vec E\ped{pol}$, dove $\vec E\ped{pol}$ è il campo generato dalla polarizzazione.



\paragraph{Energia elettrostatica in presenza di dielettrici}

Andiamo a recuperare quello che abbiamo fatto nel capitolo di elettrostatica per trovare un'espressione dell'energia elettrostatica della configurazione in termini solo del campo e non delle altre quantità come la densità di carica.

In generale, l'equazione che continuerà a valere sarà

\[U = \dfrac{1}{2}\dint_V \phi(\vec r) \rho(\vec r) \dd V \] 

In quanto per scrivere questa formula abbiamo semplicemente generalizzato quello che succede per cariche puntiformi. Sarà semplicemente opportuno adesso maneggiarla in modo da far comparire qualcosa che ci può essere utile alla luce della nuova equazione

\[ \div \vec D = \rho\ped{lib}\]

Per cui 

\[U = \dfrac{1}{2}\dint_V \phi \div \vec D  \dd V = \dfrac{1}{2}\dint_V \div(\phi \vec D) \dd V - \dfrac{1}{2}\dint_V \grad \phi \cdot \vec D  \dd V  \]

E, al solito, se la distribuzione di carica è localizzata il primo termine va a zero quando si fa il limite su tutto lo spazio, per cui rimane solo il secondo termine che è 

\[U = \dint_V \dfrac{\vec E \cdot \vec D}{2} \dd V \]




\paragraph{Condensatori con dentro dielettrici}
L'applicazione più banale dei modelli che abbiamo fatto fin'ora. Consideriamo per esempio un condensatore piano a facce parallele. Mettiamo all'interno del volume fra le facce un dielettrico lineare caratterizzato da una costante dielettrica relativa $\epsilon_r$. Separiamo una carica $Q$ sulle facce del condensatore e vediamo cosa succede (Ovvero su una faccia ci sarà $Q$ e sull'altra $-Q$)







\begin{figure}[!ht]
\centering
\includegraphics[width = 0.5 \textwidth]{immagini/capacitore_dielettrico_2.png}
\caption{Condensatore con un dielettrico all'interno}
\label{fig:condensatore dielettrico}
\end{figure}


Applichiamo il teorema di teorema del flusso di Gauss al vettore $\vec D$ per una superficie fatta così: il bordo di un parallelepipedo con una faccia immersa nel conduttore e l'altra immersa nel dielettrico. Ovviamente le altre facce non contribuiscono al flusso in quanto se non consideriamo gli effetti di bordo il campo diventa parallelo alla superficie. Dato che abbiamo scelto $\vec D$ proprio in modo da poter considerare solo la carica libera, il teorema di Gauss ci dice che

\[\Phi(\vec D, \d V) = Q \Rightarrow \epsilon_0\epsilon_r\Phi_1(\vec E, \d V) + \epsilon_0 \Phi_2(\vec E, \d V) = Q \Rightarrow E = \dfrac{Q}{A\epsilon_0\epsilon_r} \]


Nei passaggi precedenti abbiamo indicato con $\Phi_1$ il flusso attraverso la superficie del dielettrico e con $\Phi_2$ il flusso attraverso il conduttore. Abbiamo usato che in un dielettrico lineare si ha $\vec D = \epsilon_r \epsilon_0 \vec E$ e che al solito in un conduttore all'equilibrio il campo elettrico è nullo. Notiamo che se vogliamo andare a calcolare la capacità di un condensatore fatto in questo modo

\[C = \dfrac{Q}{\Delta V} \Rightarrow C = \dfrac{Q}{Ed} = \dfrac{Q}{\frac{Q}{A\epsilon_0\epsilon_r}} = \epsilon_0\epsilon_r \dfrac{A}{d} = \epsilon_r C_0  \]

Dove ho indicato con $C_0$ la capacità di un condensatore della stessa forma ma con il vuoto all'interno. Notiamo che dato che $\epsilon_r \geq 1$, la capacità è maggiore di quella che si avrebbe normalmente. Ricordatevi che se avete a che fare con un condensatore dove al posto del vuoto c'è ovunque un dielettrico, in generale la capacità sarà $\epsilon_r$ volte la capacità di quello vuoto.

Vediamo un pochino di Fisica di questo modello. Notiamo intanto che il campo elettrico che abbiamo calcolato

\[E = \dfrac{Q}{A\epsilon_0\epsilon_r} = \dfrac{E_0}{\epsilon_r}\]

È minore di quello che si avrebbe normalmente. Possiamo interpretare fisicamente questo risultato in modo molto semplice guardando la figura \ref{fig:condensatore dielettrico}. Infatti è evidente che sull'armatura con carica positiva i dielettrici si dispongono in modo da mostrare il lato negativo, creando una sorta di $\sigma$ che si aggiunge alla precedente, ma di segno opposto. Di conseguenza il campo elettrico all'interno sarà minore.


Supponiamo ora di avere un condensatore isolato su cui c'è una carica separata $Q$. Cosa succede all'energia interna del condensatore? 

Al solito potremo usare la formula

\[U = \dint_V \dfrac{\vec D \cdot \vec E}{2} \dd V  = \dint_V\dfrac{\frac{Q}{A\epsilon_0\epsilon_r}\dfrac{Q}{A}}{2} \dd V = \dfrac{Q^2}{2A^2\epsilon_0\epsilon_r} Ad = \dfrac{Q^2}{2C}\]

E anche stavolta vale la formula per l'energia interna del condensatore. Notiamo che l'energia interna a parità di carica separata è minore di quella che avrebbe se le armature avessero il vuoto all'interno. Di conseguenza, una lastra di dielettrico verrebbe risucchiata dalle armature di un condensatore carico. 



















\paragraph{Condizioni al bordo}
Ricordiamo che nella parte di elettrostatica avevamo parlato rapidamente delle condizioni sul campo elettrico sui bordi di discontinuità. In particolare, se su una certa superficie è presente una densità di carica superficiale $\sigma(\vec r)$, allora il vettore campo elettrico avrà una discontinuità a salto descritta matematicamente dalle formule

\[
\begin{cases}
(\vec E(\vec r_+) - \vec E(\vec r_-) )\cdot \b n = \frac{\sigma(\vec r)}{\epsilon_0} \\
(\vec E(\vec r_+) - \vec E(\vec r_-)) \times \vec n = 0 \\
\end{cases}
\]



Le formule che ho scritto sembrano brutte ma in realtà ci dicono solo che la componente del campo elettrico perpendicolare alla superficie salta di una certa quantità ben determinata, mentre le due componenti parallele al piano devono per forza rimanere inalterate. Queste formule non sono frutto di grosse elucubrazioni, abbiamo visto che derivano quasi direttamente dalla conservatività del campo elettrico e dal teorema di Gauss. L'implicazione è quindi


\[ 
\begin{cases}
\div \vec E = \frac{\rho}{\epsilon_0} \\
\rot \vec E = 0 \\
\end{cases}
\Rightarrow
\begin{cases}
(\vec E(\vec r_+) - \vec E(\vec r_-) )\cdot \b n = \frac{\sigma(\vec r)}{\epsilon_0} \\
(\vec E(\vec r_+) - \vec E(\vec r_-)) \times \vec n = 0 \\
\end{cases}
\]

A questo punto non ci vuole un genio per capire che se rimpiazziamo quella versione del teorema di Gauss con la nuova versione applicata ai materiali si ottiene semplicemente


\[ 
\begin{cases}
\div \vec D = \rho\ped{lib} \\
\rot \vec E = 0 \\
\end{cases}
\Rightarrow
\begin{cases}
(\vec D(\vec r_+) - \vec D(\vec r_-) )\cdot \b n = \sigma\ped{lib} \\
(\vec E(\vec r_+) - \vec E(\vec r_-)) \times \vec n = 0 \\
\end{cases}
\]



Queste sono le condizioni di raccordo che ci dicono come varia il campo elettrico subito fuori e subito dentro la superficie di separazione di un materiale dielettrico. Per vedere come le cose tornino, riprendiamo l'esempio del condensatore a facce piane e parallele che abbiamo visto poco fa, con una leggera modifica. Riempiamo il volume con il dielettrico solo per metà, in modo che una delle due facce sia coperta di dielettrico mentre l'altra non abbia niente davanti, in modo che lo strato di dielettrico sia spesso $d$ e lo strato di vuoto altrettanto, per una distanza totale fra le piastre di $2d$.

L'obiettivo è, data la carica $Q$ separata sulle piastre, trovare il campo elettrico in tutto lo spazio compreso fra le due armature. Per farlo possiamo appunto usare le formule che abbiamo appena ricavato, per provare a vedere come usarle. Consideriamo la superficie di separazione fra il conduttore e il dielettrico. Possiamo usare le formule che abbiamo scritto poco sopra per dire immediatamente\footnote{Grazie al fatto che in un conduttore in elettrostatica il campo elettrico sappiamo sempre quanto fa, ovvero 0} che 

\[ E_1 = \dfrac{Q}{A \epsilon_0\epsilon_r}\]

Ovvero il campo elettrico all'interno del dielettrico è minore di quello che ci sarebbe senza, come ci aspettavamo. Per calcolare il campo elettrico davanti all'armatura senza dielettrico si può usare la stessa identica formula e ottenere subito che

\[E_2 = \dfrac{Q}{A\epsilon_0} \]

Che era il risultato consueto. Ora controlliamo se le cose tornano anche sulla superficie intermedia, ovvero quella fra il dielettrico e il vuoto. Su quella superficie non vi è carica elettrica \textit{libera}, la carica superficiale che si viene a formare è una carica di polarizzazione indotta, per cui sulla superficie avremo $\sigma = 0$. Di conseguenza, dovrà essere

\[(\vec D(\vec r_+) - \vec D(\vec r_-))\cdot \b n = 0 \]

Ovvero, dato che il campo ha sempre la stessa direzione 

\[ D(\vec r_+) = D(\vec r_-)\]


Da una parte abbiamo il vuoto, per cui $D_2 = \epsilon_0 E_2$, mentre dall'altra abbiamo un dielettrico lineare, per cui effettivamente $D_1 = \epsilon_0\epsilon_r E_1$. La formula che abbiamo appena scritto ci dice che deve valere

\[ \epsilon_0\epsilon_r E_1 = \epsilon_0 E_2\]


E non è difficile accorgersi che le soluzioni che abbiamo trovato per i due campi soddisfano effettivamente questa relazione. 

Calcoliamo per completezza la ddp ai capi del condensatore, cosa indispensabile per il calcolo della capacità. Il potenziale, come in elettrostatica, è sempre definito come

\[\Delta V_{ab} = - \dint_a^b \vec E \cdot \dd\vec r \]

Nel nostro caso, a meno di un segno che per il calcolo della capacità non è importante, la ddp sarà 

\[ |\Delta V| = E_1 d + E_2 d = \dfrac{Qd}{A\epsilon_0} \left(1 + \dfrac{1}{\epsilon_r}\right)\]

Per cui 

\[ C = \dfrac{Q}{|\Delta V|} = \epsilon_0\dfrac{A}{d} \dfrac{\epsilon_r}{1+\epsilon_r}\]

Vorrei far notare che torna il caso limite $\epsilon_r \to 1$ in quanto poi la capacità tende a $\epsilon_0 \frac{A}{2d}$.


\newpage
\subsubsection{Magnetostatica nei materiali}
Avrete cominciato ad intuire che il campo elettrico e il campo magnetico si assomigliano abbastanza. Per questo motivo il modello di base da cui si parte per la magnetostatica nei materiali è esattamente lo stesso che si ha in elettrostatica, ovvero si parte dall'ipotesi \textit{approssimata ma ragionevole} che un materiale con caratteristiche magnetiche possieda una densità di momento di dipolo magnetico $\vec M$, perfettamente analogo a $\vec P$, ovvero se vale

\[ \vec p = \dint_V \vec P  \dd V\]

Allora vale 

\[ \vec \mu = \dint_V \vec M  \dd V\]

Analogamente a quanto abbiamo fatto per il potenziale scalare elettrico, faremo uguale ora per il potenziale vettore magnetico cercando di trovare una relazione nuova fra $\vec B, \vec M, \vec J$, come abbiamo fatto con il campo elettrico trovando una formula nuova fra $\vec E, \vec P, \rho$


Ricordiamo qui la formula del potenziale vettore generato da un dipolo matematico VAI A DIMOSTRARLA NEL CAPITOLO DI MAGNETOSTATICA CHE NON LO HAI MAI FATTO

\[\vec A = \dfrac{\mu_0}{4\pi}\dfrac{\vec m \times \b r}{r^2}\]

Per cui l'ovvia generalizzazione sarà 

\[\vec A(\vec r) = \dfrac{\mu_0}{4\pi} \dint_V \dfrac{\vec M(\vec r') \times (\vec r - \vec r')}{|\vec r - \vec r'|^3} \dd V\]

Ora come al solito usiamo una delle identità vettoriali a caso che non verrebbero mai in mente ma che servono per fare manipolazioni intelligenti


\[ \rot (f\vec F) = \grad f \times \vec F + f \rot \vec F\]


E, tanto per cambiare, usiamo la solita identità

\[\dfrac{\vec r - \vec r'}{|\vec r - \vec r'|^3} = \grad' \dfrac{1}{|\vec r - \vec r'|} \]

Per cui l'identità con il rotore si scrive

\[ \vec \nabla ' \times \left(\dfrac{\vec M(\vec r')}{|\vec r - \vec r'|}\right) = \grad' \dfrac{1}{|\vec r - \vec r'|} \times \vec M(\vec r') + \dfrac{\vec \nabla ' \times  \vec M(\vec r')}{|\vec r - \vec r'|}\]


Per cui, ricordando che il prodotto vettore è anticommutativo, la formula per il potenziale vettore diventa


\[\vec A(\vec r) = \frac{\mu_0}{4\pi} \dint_V\left(-\vec \nabla ' \times \left(\dfrac{\vec M(\vec r')}{|\vec r - \vec r'|}\right)  + \dfrac{\vec \nabla '\times \vec M(\vec r')}{|\vec r - \vec r'|}\right) \dd V \]


A questa formula dobbiamo aggiungerci il potenziale vettore generato dalle correnti vere, ovvero dobbiamo aggiungere il termine che contiene $\vec J$, che sappiamo valere

\[\dfrac{\mu_0}{4\pi}\dint_V \dfrac{\vec J(\vec r')}{|\vec r - \vec r'|} \dd V \]

Per cui, riscrivendo la formula del potenziale in modo più sensato, 


\[\vec A(\vec r) = \frac{\mu_0}{4\pi} \left(\dint_V \dfrac{\vec J(\vec r') + \vec \nabla'\times \vec M(\vec r')}{|\vec r - \vec r'|}  \dd V - \dint_V \vec \nabla ' \times \left(\dfrac{\vec M(\vec r')}{|\vec r- \vec r'|}\right) \dd V\right) \]


Il secondo termine è un termine di bordo. Usando il teorema di Stokes può essere scritto in modo più concreto come
\[\vec A(\vec r) = \frac{\mu_0}{4\pi} \left(\dint_V \dfrac{\vec J(\vec r') + \vec \nabla'\times \vec M(\vec r')}{|\vec r - \vec r'|}  \dd V - \doint_{\d V}  \dfrac{\vec M(\vec r')}{|\vec r- \vec r'|}\times \dd\vec A\right) \]


In pratica ci sta dicendo che sul bordo vi è una corrente superficiale il cui contributo al potenziale vettore sarà quello. Cerchiamo di usare la formula precedente per trovare una relazione fra $\vec B, \vec M, \vec J$. Buttando via il termine di bordo\footnote{Ci sono motivi per farlo ma la loro discussione non è utile a questo livello. Voi ricordatevi della presenza del termine di bordo per il calcolo dei campi, ma dimenticatevene quando fate le dimostrazioni. Avrete tempo all'università per capire perché possiamo ignorarlo ogni volta.}, possiamo notare che la formula per il potenziale vettore è uguale a quella classica se non per la presenza di un termine che può essere visto come una $\vec J\ped{eff}$, ovvero

\[\vec J\ped{eff} = \vec J + \rot \vec M\]

Ma noi conosciamo una relazione fra $\vec B$ e $\vec J\ped{eff}$, per analogia al caso senza i materiali, ovvero

\[\rot \vec B = \mu_0 \vec J\ped{eff} = \mu_0 (\vec J + \rot \vec M)\]


Che si riscrive in modo molto semplice come

\[ \rot\left(\frac{\vec B}{\mu_0} - \vec M\right) = \vec J\]

E indovinate cosa si fa ora? Si definisce un nuovo vettore, in analogia con il campo elettrico. Il vettore $\vec H$, tale che 

\begin{equation}
\label{maxwell 4 statica materiali}
\rot \vec H = \vec J
\end{equation}


Per cui, 

\begin{equation}
\label{definizione vettore H}
\mu_0(\vec H + \vec M) = \vec B
\end{equation}


Questa relazione è sempre vera perché è in sostanza la definizione di un vettore fittizio, $\vec H$, che permette di scrivere le leggi in forma più compatta e tenere solo conto delle correnti libere in circolazione e non delle correnti indotte nei materiali. Quello che aggiunge Fisica al modello del materiale è una relazione aggiuntiva

\[f(\vec M, \vec B, \vec H) = 0 \]


Ovvero qualcosa che ci dica in che modo sono legati i tre vettori. Forse non sono stato molto chiaro, in quanto mi direte che già l'equazione \ref{definizione vettore H} è una relazione di questo tipo. Io intendo dire una nuova relazione che leghi per esempio solo $\vec M$ e $\vec B$ all'interno del mezzo, un equivalente di quello che si faceva in elettrostatica dicendo per esempio che la relazione fra $\vec D$ e $\vec E$ era lineare per una vasta gamma di materiali, chiamati dielettrici lineari. Per il caso magnetico, le cose sono più difficili. Vedremo in questo capitolo che esistono principalmente 3 tipi di risposta diversa dei materiali all'applicazione di campi magnetici esterni, ovvero

\begin{enumerate}
	\item Ferromagnetismo (quello delle calamite, il più noto)
	\item Paramagnetismo (simile come reazione a quello che accade nei dielettrici lineari)
	\item Diamagnetismo (questo è molto più peculiare del magnetismo. Al suo estremo porta al fantastico mondo dei superconduttori)
\end{enumerate}





\paragraph{Equivalenza fra campi}
Vediamo rapidamente un metodo molto furbo per trattare una vasta gamma di problemi per analogia prima di addentrarci nei modelli specifici del ferromagnetismo e delle risposte magnetiche della materia. Scriviamo le leggi di Maxwell per il caso statico come le abbiamo modificate per aggiungere i materiali. 


\[ 
\begin{cases}
\div \vec D = \rho\ped{lib} \\
\rot \vec E = 0 \\
\div \vec B = 0 \\
\rot \vec H = \vec J\ped{lib} \\
\end{cases}
\]



Poniamoci ora nella situazione semplificata in cui non vi sono cariche o correnti esterne ma solo materiali magnetizzati o polarizzati. Di conseguenza, avremo $(\rho\ped{lib}, \vec J\ped{lib}) = 0$. Le equazioni si scriveranno




\[ 
\begin{cases}
\div \vec D = 0 \\
\rot \vec E = 0 \\
\div \vec B = 0 \\
\rot \vec H = 0 \\
\end{cases}
\]

Non è difficile capire da questo sistema di equazioni la stretta analogia che lega $\vec E $ con $\vec H$ e $\vec D$ con $\vec B$. Notare che per una scelta sfortunata di convenzioni il legame è incrociato, ovvero il campo reale di uno è associato al campo fittizio dell'altro e viceversa.

Pensate sia un'analogia inutile? Beh, fate il problema \ref{prob:riciclone_magnetostatica}. Vedrete che è molto utile usare direttamente questi fatti per risolvere problemi che in realtà avete già affrontato, solo che ancora non ve ne siete accorti.





Vorrei far notare che la definizione di $\vec H$ è fatta al contrario rispetto alla definizione di $\vec D$, ovvero se confrontiamo le due definizioni

\[ 
\begin{cases}
\vec D = \epsilon_0\vec E + \vec P \\
\vec B = \mu_0 \vec H + \mu_0 \vec M \\
\end{cases}
\]


Vedete che a sinistra abbiamo $\vec D$ che è un vettore finto, che serve solo a scrivere bene le leggi, e abbiamo anche $\vec B$, che invece è il vettore vero che esercita la forza sulle cariche.




\paragraph{Paramagnetismo}
Cominciamo a dare un po' di modelli fisici in più. Il paramagnetismo è quello che assomiglia di più a quello che si è visto per l'elettrostatica. Il modello che capita sempre è ovviamente quello \textit{lineare} che ci dice

\[ \vec B = \mu_0 \mu_r \vec H \]

Dove $\mu_r$ è un numero reale positivo. 


FINISCI DI SCRIVERE	

\paragraph{Diamagnetismo}




\paragraph{Ferromagnetismo forte}








\paragraph{Superconduttori}









\newpage
\subsubsection{Campi oscillanti nella materia}







\newpage
\paragraph{Probemi}
\input{src/elettromagnetismo/problemi_materia.tex}