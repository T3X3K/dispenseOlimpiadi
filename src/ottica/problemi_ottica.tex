\begin{problemcathegory}[ottica]



\begin{problemwithsol}{Palla in fondo ad una vasca (Senigallia 2010)}{palla_vasca}\small\score{2}{5}
La profondità di una vasca è uniforme e vale $L$. L'acqua è calma e limpida.

Dal punto $O$, posto al pelo dell'acqua, si osserva un punto $A$ del fondo della vasca che dista $x$ dalla verticale passante per $O$.

\begin{enumerate}
	\item Determinare in funzione di $L,n$ ed $x$ la profondità dell'immagine virtuale del punto $A$, nel piano passante per $A$ e per $O$
	\item Determinare al variare di $x$ i valori $l\ped{min}$ ed $l\ped{max}$
	\item Determinare l'area complessiva della vasca che si può osservare dal punto $O$
\end{enumerate}

Hint: \ref{hint palla vasca}
\begin{solution}
Ricordate che in ottica geometrica la prima cosa da fare \textbf{è un disegno grande} su cui prendere appunti e poter disegnare ulteriormente. Su un disegno piccolo non si capisce niente e si sbaglia.


\begin{figure}[ht]
	\centering
		\includegraphics[width = 0.8 \textwidth]{immagini/palla_vasca.png}
	\caption{disegno del problema \ref{prob:palla_vasca}}
	\label{fig:palla_vasca}
\end{figure}


La seconda cosa da fare è mettere un sacco di lettere sulla figura in modo da dare il nome ad ogni cosa. Possibilmente non devono essere troppo simili in modo da evitare errori stupidi di copiatura. Poi, una volta fatto questo bisogna scrivere un sacco di equazioni a caso e quando si è soddisfatti bisogna cercare di andare avanti per eliminazione. È importante scrivere bene il sistema iniziale in modo da ricordarsi da quali equazioni si è partiti per evitare di tornare a scrivere la stessa equazione quando non bastano quelle già scritte.

Vediamo ora il sistema che ho scritto io.

\[
\begin{cases}
n\sin\phi = \sin \theta \\
AO \cos\phi = AO' \cos(\phi +  \dd \phi) = L \\
A'O' \cos (\theta +  \dd \theta) = A'O \cos \theta = l \\
A'O' \sin (\theta +  \dd \theta) = A'O \sin\theta +  \dd x \\
AO' \sin (\phi +  \dd \phi) = AO \sin \phi +  \dd x \\
\end{cases}
\]

Ovviamente la prima è la legge di Snell ed è l'unica legge fisica del problema. Le altre equazioni sono solo considerazioni di geometria piana. Vorrei far notare che sia $ \dd \theta$, sia $ \dd \phi$ sia $ \dd x$ andranno a 0 quando faremo il limite, quindi tenetene conto quando andate avanti.


A questo punto è opportuno far sparire delle variabili in quanto sono decisamente troppe. L'idea migliore è esprimere tutto in funzione di $L$ e di $l$ (e degli angoli), dato che uno dei due lo sappiamo e l'altro è quello che vogliamo trovare. Sperabilmente troveremo un'equazione fra i due.

Prendiamo quindi la terza equazione e ricaviamone qualcosa. Esprimiamo $A'O'$ e $A'O$ in termini di $l, \theta$ e poi sostituiamo nella quarta equazione.


\[\dfrac{l}{\cos(\theta+ \dd \theta)} \sin (\theta +  \dd \theta) = \dfrac{l}{\cos\theta} \sin\theta +  \dd x\]

Che si scrive in modo più umano come


\[
\begin{cases}
l\left(\tan(\theta +  \dd \theta) - \tan\theta\right)= \dd x \\
L\left(\tan(\phi +  \dd \phi) - \tan\phi\right)= \dd x \\
\end{cases}
\]

Dove abbiamo anche fatto la stessa cosa per $\phi, \theta$ e messo a sistema. Siamo vicini all'obiettivo. Abbiamo due cose uguali a $ \dd x$ che è comunque un'incognita, quindi tanto vale buttarlo via e porre uguali i due LHS.


\[L\left(\tan(\phi +  \dd \phi) - \tan\phi\right) = l\left(\tan(\theta +  \dd \theta) - \tan\theta\right) \]

Questa è quasi un'equazione fra $L$ e $l$. Dobbiamo fare il limite per $ \dd \theta, \dd \phi \to 0$. In particolare, questo può sembrare un limite in due variabili, in quanto mandiamo a 0 due cose contemporaneamente. Normalmente un limite doppio è molto più fastidioso di un limite singolo, ma in questo caso i due angoli sono sempre legati dall'equazione


\[n\sin\phi = \sin\theta \]


E quindi in qualche modo bisognerà tenerne conto quando si fa il limite. Fra poco vedremo in che modo. Nel frattempo, espandiamo in serie l'equazione precedente. Ricordiamo che 

\[f(x+\Delta x) - f(x) \approx \dfrac{\dd f(x)}{\dd x}\Delta x  \]

Dove abbiamo tenuto il primo termine dello sviluppo di Taylor. Vorrei far notare che questa formula è esatta e non approssimata in questo caso in quanto stiamo facendo un procedimento di limite e i termini di ordine superiore spariranno.

\[L \dfrac{1}{\cos^2\phi}  \dd \phi = l \dfrac{1}{\cos^2\theta}  \dd \theta\]


Questo è il risultato dell'espansione. Ricaviamo $l$


\[l = L \dfrac{\cos^2\theta}{\cos^2\phi}\dfrac{\dd \phi}{\dd \theta} \]

A questo punto bisogna valutare $\frac{\dd \phi}{\dd \theta}$. Vorrei far notare che se in generale questi due non fossero legati, il limite sarebbe di difficile trattazione. Per fortuna noi abbiamo a disposizione la legge di Snell


\[n\sin\phi = \sin\theta \]

Che ci permette di calcolare il rapporto. Per farlo possiamo agire in due modi assolutamente equivalenti. Il primo è quello semplice di differenziare l'equazione precedente.

\[ n\cos\phi  \dd \phi = \cos\theta  \dd \theta\]


Come ho già detto, questa tecnica può sembrare fatta a caso ma in realtà è molto formale. Può essere matematicamente giustificata attraverso il teorema delle funzioni implicite (teorema del Dini), che comunque vi sconsiglio di guardare.

L'altro metodo, molto più fastidioso, consiste nel ricavare $\phi = f(\theta)$ e farne la derivata.


\[ \phi = \arcsin\left(\dfrac{\sin\theta}{n}\right) \Rightarrow \dfrac{\dd \phi}{\dd \theta} = \dfrac{1}{\sqrt{1-\left(\frac{\sin\theta}{n}\right)^2}} \dfrac{\cos\theta}{n} = \dfrac{1}{\sqrt{1-\sin^2\phi}} \dfrac{\cos\theta}{n} = \dfrac{\cos\theta}{n\cos\phi}\]

Tuttavia vi sfido dalla prima uguaglianza ad arrivare all'ultima senza sapere già che l'espressione si semplificherà parecchio. Concludiamo infine


\[l = L \dfrac{\cos^2\theta}{\cos^2\phi}\dfrac{\cos\theta}{n\cos\phi} = \dfrac{L}{n}\left(\dfrac{\cos\theta}{\cos\phi}\right)^3\]

Che ora si potrà anche esprimere in termini solo di $x, L$. Prima di farlo vediamo un paio di casi limite fisici. Se abbiamo $n = 1$, ci aspettiamo che sia $l = L $ e in effetti la formula torna, in quanto in tal caso si ha anche automaticamente $\cos \theta = \cos\phi$

Se consideriamo il caso di raggi verticali, immagino avrete già visto che il risultato deve essere $L/n$ e, in effetti, si avrebbe $\theta, \phi \to 0$ e quindi $\cos\theta, \cos\phi \to 1$




A questo punto per trovare $l\ped{min}, l\ped{max} $ basta fare un banale studio di funzione.

\[l = \dfrac{L}{n}\left(\dfrac{1-\sin^2\theta}{1-\sin^2\phi}\right)^{\frac{3}{2}} = \dfrac{L}{n}\left(\dfrac{n^2(1-\sin^2\theta)}{n^2-\sin^2\theta}\right)^{\frac{3}{2}} \]

Che per la monotonia di $x^{\frac{3}{2}}$ rende lo studio equivalente a studiare

\[f(x) = \dfrac{1-x}{n^2 - x} \qquad x\in [0,1]\]


Si vede subito che $f' <0$ in $[0,1]$ e quindi avremo il massimo in 0 e minimo in 1


Per l'ultima domanda è necessario esprimere la formula fra $l$ ed $L$ in funzione di $x$. L'impresa non è ardua in quanto 

\[\sin \phi = \dfrac{x}{\sqrt{L^2+x^2}}\]

E con un po' di conti si ottiene

\[l = \dfrac{L}{n}\left(1-(n^2-1)\left(\frac{x}{L}\right)^2\right)^\frac{3}{2}\]

Chiaramente la radice non può essere negativa e quindi il valore massimo di $x$ si avrà per 

\[x\ped{max}^2 = \dfrac{L^2}{n^2-1}\]

Notare che per $n = 1$ effettivamente si vede tutta la superficie del lago.


\end{solution}

\end{problemwithsol}




\begin{problemwithsol}{Levitazione laser (IPhO 1993)}{ipho_1993_laser}

\end{problemwithsol}





\begin{problemwithsol}{Pesce rosso}{pesce_rosso}

\end{problemwithsol}


\begin{problemwithsol}{Arcobaleno}{arcobaleno}


\end{problemwithsol}



\begin{problemwithsol}{Birifrangenza}{birifrangenza}\small\score{3.5}{5}
  Questo problema non è complicato in sé ma può risultare ostico in quanto utilzza strumenti matematici a voi poco familiari. Non scoraggiatevi. Per motivarvi a farlo, vi dico che è un testo d'esame preso da \cite{la_rocca}.
  
  Si consideri un mezzo dielettrico trasparente ($\epsilon \in \R$), uniassiale per il quale $\epsilon_{xx} = \epsilon_{yy} = \epsilon_{\perp}$, $\epsilon_{zz} = \epsilon_{//}$, $\epsilon_{i\neq j} = 0$, con $\epsilon_{//} > \epsilon_\perp > 1$  e indipendenti dalla frequenza. Supporre che i campi elettrico e magnetico dipendano dalla frequenza come

  \[
    e^{i(kx\sin\theta + kz \cos \theta - \omega t) }
  \]
  Si calcoli la relazione che lega $k$ e $\omega$, \textbf{precisandone la dipendenza dalla polarizzazione}. Si determini la direzione lungo la quale ha luogo la propagazione di energia elettromagnetica.
  A fine problema si legga la soluzione per una interpretazione fisica del risultato.
\end{problemwithsol}






\begin{problemwithsol}{Strane riflessioni}{non_sbagliare_quel_segno}\small\score{3.5}{5}
  Anche questo è un testo d'esame preso da \cite{la_rocca}. Non vi scoraggiate.
  Si consideri un ipotetico mezzo materiale omogeneo ed isotropo che, in una determinato intervallo di $\omega $ abbia una costante dielettrica $\epsilon$ e una costante magnetica $\mu$ scalari, indipendenti dalla frequenza, reali \textbf{ed entrambe negative}.
  Si studi qualitativamente la propagazione di onde piane all'interno di un mezzo infinito di questo tipo, precisando in particolare la relazione fra $\vec k, \vec E, \vec H$ e la relazione fra $\vec k$ e $\vec S$.
  Si determini l'angolo con cui si rifrange un raggio luminoso ad un'interfaccia piana fra questo mezzo e il vuoto, possibilmente illustrando il tutto con un disegno.
\end{problemwithsol}

\end{problemcathegory}