#!/usr/bin/env python
# -*- coding:utf-8 -*-

import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit


temperature = np.array([273, 283, 293, 303, 313, 323, 333, 343, 373])
viscosita = np.array(
    [1.681, 1.621, 1.552, 1.499, 1.450, 1.407, 1.367, 1.327, 1.232])


def maxwell(T, C, E_0):
    return C*np.exp(E_0/T)


def retta(x, m, q):
    return m*x+q


pars, covm = curve_fit(maxwell, temperature, viscosita)
C, E_0 = pars
dC, dE_0 = np.sqrt(covm.diagonal())

print('mu_0 = %f +- %f' % (C, dC))
print('E_0 = %f +- %f ' % (E_0, dE_0))

plt.figure(1)
plt.grid(color='gray')
# plt.subplot(2,1,1)
plt.title('Non rettificato')
plt.xlabel('Temperatura [K]')
plt.ylabel('Viscosita [10^{-3} Pa s]')
# plt.errorbar(temperature, viscosita, linestyle = '', marker = 'o')
x = np.linspace(min(temperature), max(temperature), 1000)
y = maxwell(x, C, E_0)
# plt.plot(x,y)

# plt.subplot(2,1,2)
Y = np.log(viscosita)
X = 1./temperature
plt.errorbar(X, Y, linestyle='', marker='o')
plt.title('Rettificato')
plt.xlabel('X [K^-1]')
plt.ylabel('Y [ad]')

pars, covm = curve_fit(retta, X, Y)
m, q = pars
dm, dq = np.sqrt(covm.diagonal())
print('m = %f +- %f' % (m, dm))
print('q = %f +- %f' % (q, dq))

x = np.linspace(0, max(X), 1000)
y = retta(x, m, q)
plt.plot(x, y)


out_file = open('out_table.txt', 'w')

for i in range(1, len(X)):
    out_file.write('$ %.5f $ & $ %.3f $ \\\\ \n ' % (X[i], Y[i]))
    out_file.write('\hline \n')


out_file.close()
